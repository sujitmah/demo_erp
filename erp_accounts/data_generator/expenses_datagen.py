import csv
from ..journal.models import Journal


class LoadExpenses(object):
	"""docstring for LoadExpenses"""
	def __init__(self, arg):
		super(LoadExpenses, self).__init__()
		self.arg = arg

	def load(self):

		with open('data/expenses.csv') as csvfile:
		    reader = csv.DictReader(csvfile)
		    for row in reader:
		    	amount = int(row.get("amount").strip())
		    	ledger_debit = row.get("ledger")
		    	ledger_credit = "NIBL"
		        transactions = [
                {
                    "amount": amount,
                    "transaction_type": "debit",
                    "ledger": ledger_debit
                }, 
                {
                    "amount": amount,
                    "transaction_type": "credit",
                    "ledger": ledger_credit
                }
	            ]
	            # Journal.create_journal(
	            #     transactions=transactions,
	            #     journal_date="2016-02-05T18:15:00.000Z",
	            #     description="Free Payment for - {0}".format(student_name,)
	            # )
			       