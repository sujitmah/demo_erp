import datetime
from ..coaccounts.models import FiscalYear, AccountGroup


class CoaAccountDataGen:
    """docstring for CoaAccountDataGen"""

    def load_initial_groups(self):
        FiscalYear.objects.create(
            fiscal_year_from=datetime.datetime.now().date(),
            fiscal_year_to=datetime.datetime.now().date(),
        )
        primary_groups = [
            'Income',
            'Expenses',
            'Assets',
            'Liabilities'
        ]
        secondary_groups = {
            "Assets": [
                'Bank Accounts',
                'Bank OCC A/C',
                'Bank OD A/C',
                'Branch / Divisions',
                'Cash-in-hand',
                'Current Assets',
                'Fixed Assets',
                'Deposits (Asset)',
                'Investments',
                'Misc. Expenses (ASSET)',
                'Retained Earnings',
                'Reserves & Surplus',
                'Stock-in-hand',
            ],
            "Liabilities": [
                'Capital Account',
                'Current Liabilities',
                'Loans & Advances (Asset)',
                'Loans (Liability)',
                'Provisions',
                'Sundry Creditors',
                'Sundry Debtors',
                'Unsecured Loans',
                'Secured Loans',
            ],
            "Income": [
                'Direct Incomes',
                'Income (Direct)',
                'Indirect Incomes',
                'Income (Indirect)',
                'Export',
                'Sales Accounts',
            ],
            "Expenses": [
                'Direct Expenses',
                'Duties & Taxes',
                'Expenses (Direct)',
                'Expenses (Indirect)',
                'Indirect Expenses',
                'Suspense A/C',
                'Purchase Accounts',
            ]
        }

        for primary_group in primary_groups:
            fiscal_year = FiscalYear.objects.all()[0]
            primary_object = AccountGroup(
                name=primary_group, parent=None, fiscal_year=fiscal_year)
            primary_object.save()
            for secondary_group in secondary_groups[primary_group]:
                secondary_object = AccountGroup(
                    name=secondary_group,
                    parent=primary_object, fiscal_year=fiscal_year)
                secondary_object.save()
