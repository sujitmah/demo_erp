"""
1. Load Sujit's initial data.
2. Add new ledgers.
3. Create Transactions
"""

from ..journal.models import Journal

from names import BOYS_NAMES


class StudentDataGen:
    bank_accounts = ['NIBL', 'Vibor Bank', 'Everest']
    years = ['2071', '2072']
    students = BOYS_NAMES

    def __init__(self, **kwargs):
        for key, value in kwargs.items():
            setattr(self, key, value)

    def generate_student_ledger(self, total, amount=30000):
        if total > len(self.students):
            print("total exceeds number of students")
            return 0
        for i in range(total):
            student_name = self.students[i]
            ledger_debit = 0
            ledger_credit = 0
            transactions = [
                {
                    "amount": amount,
                    "transaction_type": "debit",
                    "ledger": ledger_debit
                }, {
                    "amount": amount,
                    "transaction_type": "credit",
                    "ledger": ledger_credit
                }
            ]
            Journal.create_journal(
                transactions=transactions,
                journal_date="2016-02-05T18:15:00.000Z",
                description="Free Payment for - {0}".format(student_name,)
            )
