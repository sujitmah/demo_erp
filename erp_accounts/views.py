from django.views.generic import View
from django.shortcuts import render_to_response, HttpResponse
from django.template import RequestContext

class DashboardView(View):

    template_name = "accounts_dashboard.html"

    # send the user back to their own page after a successful update
    def get(self, request, *args, **kwargs):
        return render_to_response(
            self.template_name,
            {},
            context_instance=RequestContext(request)
        )

    # def get_object(self):
    #     # Only get the User record for the user making the request
    #     return User.objects.get(username=self.request.user.username)
