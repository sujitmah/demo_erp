journalApp.directive('transactions', function() {
    return {
        restrict: 'E',
        templateUrl: '/static/erp_accounts/journal/angular/templates/transactions.html',
        controller: function($scope) {

            $scope.addTransaction = function() {
                $scope.journalForm.transactions.push({
                    amount: $scope.calculateTotal()['difference'],
                    transaction_type: $scope.calculateTotal()['smaller'],
                    transaction: 'everest',
                    id: $scope.journalId
                });
                $scope.journalId += 1;
            }

            $scope.removeJournal = function(journalId) {
                $scope.journalForm.transactions = $scope.journalForm.transactions.filter(function(x) {
                    return x.id != journalId;
                });

            }

            $scope.isDebit = function(transaction_type) {
                return transaction_type == 'debit';
            }

        } // end controller function
    } //end return
});


journalApp.directive('chequeReceipts', function() {
    return {
        restrict: 'E',
        templateUrl: '/static/erp_accounts/journal/angular/templates/cheque-receipts.html',
        controller: function($scope) {
            
            $scope.addCheckReceipt = function(transaction){
                chequeReceipt = {
                    "transaction_type": "",
                    "amount": "",
                    "instrument_no": "",
                    "received_from": "",
                    "instrument_date": "",
                    "bank": "",
                    "branch": ""
                }
                if (!transaction.cheque_receipts){
                    transaction.cheque_receipts =[];
                }
                transaction.cheque_receipts.push(chequeReceipt);
            }

        } // end controller function
    } //end return
});
