journalApp.service('JournalService', function($http) {

	this.submitJournal = function(data, method, url){
        return $http({
            method: method,
            data: data,
            url: url,
            headers: {
                'Content-Type': 'application/json'
            }
        });
	}

    this.getData = function(url){
        return $http({
            method: 'GET',
            url: url
        });
    }
});