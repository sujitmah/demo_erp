journalApp.controller('JournalController',
    function($scope, JournalService) {

        $scope.journalForm = {};
        $scope.journalForm.transactions = [];
        $scope.journalId = 0;

        if (globalVars.method != "POST") {
            JournalService.getData(globalVars.ajax_url).success(function(data) {
                data.journal_date = new Date(data.journal_date);
                $scope.journalForm = data;
                var all_id = $scope.journalForm.transactions.map(function(x){
                    return x.id;
                });
                $scope.journalId = Math.max.apply(0, all_id) + 1;
                
            })
        }

        $scope.calculateTotal = function() {
            var transactions = $scope.journalForm.transactions;
            var total = {
                'smaller': 'debit',
                'debit': 0,
                'credit': 0,
                'difference': 0
            }
            for (var transaction in transactions) {
                total[transactions[transaction].transaction_type] += parseFloat(transactions[transaction].amount);
            }
            if (total['credit'] < total['debit']) {
                total['smaller'] = 'credit';
            } else if (total['debit'] < total['credit']) {
                total['smaller'] = 'debit';
            }
            total['difference'] = Math.abs(total['debit'] - total['credit'])
            return total;
        }


        $scope.formValid = function() {
            var total = $scope.calculateTotal();
            if (total['debit'] == total['credit']) {
                $scope.formError = ""
                return true;
            } else {
                $scope.formError = "Debit is not equal to credit"
                return false;
            }
        }

        $scope.submitForm = function() {
            $scope.$broadcast('show-errors-check-validity');
            if ($scope.formValid() && $scope.newForm.$valid) {
                return JournalService.submitJournal(
                    $scope.journalForm, globalVars.method, globalVars.ajax_url
                );
            }
        }


    });
