var journalApp = angular.module('journalApp',[
	'ngRoute', 'angular-progress-button-styles', 'ui.bootstrap.showErrors']);

journalApp.config(['$httpProvider', function($httpProvider) {
    $httpProvider.defaults.xsrfCookieName = 'csrftoken';
    $httpProvider.defaults.xsrfHeaderName = 'X-CSRFToken';    }
]);