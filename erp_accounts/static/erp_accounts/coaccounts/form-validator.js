var formElement = {};
$("input").on("keyup change",function(e){
	formElement.element = e.target;
	formElement.element.parentElement.classList.remove("has-danger");
	formElement.element.parentElement.classList.remove("has-success");

	if(formElement.element.checkValidity())
	{

		formElement.element.parentElement.classList.add("has-success");
		formElement.element.classList.add("form-control-success");
	}
	else
	{
		formElement.element.parentElement.classList.add("has-danger");
		formElement.element.classList.add("form-control-danger");
	}
	console.log(formElement.element);
	// $(formElement.element).checkValidity();
});