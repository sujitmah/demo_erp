
var substringMatcher = function(strs) {
  return function findMatches(q, cb) {
    var matches, substringRegex;

    // an array that will be populated with substring matches
    matches = [];

    // regex used to determine if a string contains the substring `q`
    substrRegex = new RegExp(q, 'i');

    // iterate through the pool of strings and for any string that
    // contains the substring `q`, add it to the `matches` array
    $.each(strs, function(i, str) {
      if (substrRegex.test(str)) {
        matches.push(str);
      }
    });

    cb(matches);
  };
};

// var states = [
//   'Income - Primary',
//   'Expenses - Primary',
//   'Assets - Primary',
//   'Liabilities - Primary',
//   'Bank Accounts',
//   'Bank OCC A/C',
//   'Bank OD A/C',
//   'Branch / Divisions',
//   'Capital Account',
//   'Cash-in-hand',
//   'Current Assets',
//   'Current Liabilities',
//   'Deposits (Asset)',
//   'Direct Expenses',
//   'Direct Incomes',
//   'Duties & Taxes',
//   'Expenses (Direct)',
//   'Expenses (Indirect)',
//   'Export',
//   'Fixed Assets',
//   'Income (Direct)',
//   'Income (Indirect)',
//   'Indirect Expenses',
//   'Indirect Incomes',
//   'Investments',
//   'Loans & Advances (Asset)',
//   'Loans (Liability)',
//   'Misc. Expenses (ASSET)',
//   'Provisions',
//   'Purchase Accounts',
//   'Reserves & Surplus',
//   'Retained Earnings',
//   'Sales Accounts',
//   'Secured Loans',
//   'Stock-in-hand',
//   'Sundry Creditors',
//   'Sundry Debtors',
//   'Suspense A/C',
//   'Unsecured Loans',
//   ];

// $('#the-basics .typeahead').typeahead({
//   hint: true,
//   highlight: true,
//   minLength: 1
// },
// {
//   name: 'states',
//   source: substringMatcher(groups)
// });

function group_format (group) {
  // if (group.id) { return group.text; }
       return group.name;    
    }


function matchStart (term, text) {
console.log("term : " +term);
console.log("text : " +typeof(text));
  if (text.toUpperCase().indexOf(term.toUpperCase()) == 0) {
    return true;
  }
 
  return false;
}
 


var coaccoutgrouplist_url = "/erp-accounts/coaccounts/api/accountgroup/list";
$.getJSON(coaccoutgrouplist_url,function(group_list){
//   $.fn.select2.amd.require(['select2/compat/matcher'], function (oldMatcher) {

  var duplicate_group = [];
  for(var i=0;i<group_list.length;i++)
  {
    duplicate_group.push({id:group_list[i].id, text:group_list[i].name});
  }
  
// });
   $("#parent").select2({
  data: duplicate_group
  // matcher: oldMatcher(matchStart),
  // templateSelection: group_format,
   // templateResult: group_format
  });

});

// $(".js-example-data-array").select2({
//   data: data
// })
 
