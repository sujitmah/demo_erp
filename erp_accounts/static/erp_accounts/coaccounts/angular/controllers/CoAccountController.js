coAccountApp.controller('CoAccountController',
 function($scope, CoAccountService) {
      
	$scope.groupForm = {};
	
	$scope.formValid = function(){

		if($scope.groupForm.parent > 0 && $scope.groupForm.name.length>0){
			return true
		}
		return false;
	};

	$scope.submitForm = function(){
		console.log("group_gone");
		$scope.groupForm.parent = parseInt(document.getElementById("parent").value);
		if ($scope.formValid()){
			CoAccountService.submitCoAccount($scope.groupForm);
			$scope.groupForm = {};
		}
		
	}


});


coAccountApp.controller('LedgerController',
 function($scope, CoAccountService) {
      
	$scope.ledgerForm = {};
	
	$scope.ledgerFormValid = function(){
		
		console.log("inside submitFormvalid");
		if($scope.ledgerForm.account_group > 0 && $scope.ledgerForm.name.length>0){
			return true;
		}
		return false;
	};

	$scope.submitForm = function(){
		console.log("inside submitForm");
		$scope.ledgerForm.account_group = parseInt(document.getElementById("account_group").value);
		if ($scope.ledgerFormValid()){
			console.log("inside if submitForm");
			console.log($scope.ledgerForm.account_group);
			CoAccountService.submitLedger($scope.ledgerForm);
			$scope.ledgerForm = {};
		}
		
	}


});


coAccountApp.controller('CompanyInfoController',
 function($scope, CoAccountService) {
 	
	$scope.companyForm = {};
	
	$scope.companyInfoFormValid = function(){
		return true;
		console.log("inside submitFormvalid");
		if($scope.ledgerForm.account_group > 0 && $scope.ledgerForm.name.length>0){
			return true;
		}
		return false;
	};

	$scope.submitForm = function(){
		if ($scope.companyInfoFormValid()){
			CoAccountService.submitCompanyInfo($scope.companyForm).then(function successCallback(response) {
			    window.location = "/erp-accounts/coaccounts/company/";
			  }, function errorCallback(response) {
			    // called asynchronously if an error occurs
			    // or server returns response with an error status.
			  });
			$scope.companyForm = {};
		}
		
	};


});


coAccountApp.controller('CompanyInfoDisplayController',
 function($scope, CoAccountService) {
 	$scope.companyForm = {};
 	
 	// $.getJSON("/erp-accounts/coaccounts/api/company/1",function(company){
 	// 	console.log(company);
 	// 	$scope.companyForm.name = "company";
 	// 	aa = $scope;
 	// })

 	CoAccountService.getCompanyInfo().then(function(company) {
	    //resolve the promise as the data
	    // console.log(company);
	    $scope.companyForm = company.data;
	});
 



});

coAccountApp.controller('CompanyInfoUpdateController',
 function($scope, CoAccountService) {
 	$scope.companyForm = {};
 	CoAccountService.getCompanyInfo().then(function(company) {
	    //resolve the promise as the data
	    // console.log(company);
	    $scope.companyForm = company.data;
	});
	$scope.companyInfoFormValid = function(){
		return true;
		console.log("inside submitFormvalid");
		if($scope.ledgerForm.account_group > 0 && $scope.ledgerForm.name.length>0){
			return true;
		}
		return false;
	};

	$scope.submitForm = function(){
		if ($scope.companyInfoFormValid()){
			CoAccountService.updateCompanyInfo($scope.companyForm).then(function successCallback(response) {
			    window.location = "/erp-accounts/coaccounts/company/";
			  }, function errorCallback(response) {
			    // called asynchronously if an error occurs
			    // or server returns response with an error status.
			  });
		}
		
	};
});

coAccountApp.controller('FiscalYearController',
 function($scope, CoAccountService) {
 	$scope.fiscalYearForm = {};
 	// CoAccountService.getCompanyInfo().then(function(company) {
	    //resolve the promise as the data
	    // console.log(company);
	    // $scope.companyForm = company.data;
	// });
	$scope.fiscalYearFormValid = function(){
		return true;
		console.log("inside submitFormvalid");
		if($scope.ledgerForm.account_group > 0 && $scope.ledgerForm.name.length>0){
			return true;
		}
		return false;
	};

	$scope.submitForm = function(){
		$scope.fiscalYearForm.fiscal_year_from = BS2AD($("#fiscal_year_from").val());
		$scope.fiscalYearForm.fiscal_year_to = BS2AD($("#fiscal_year_to").val());
		if ($scope.fiscalYearFormValid()){
			CoAccountService.addFiscalYear($scope.fiscalYearForm).then(function successCallback(response) {
			    window.location = "/erp-accounts/coaccounts/api/fiscal-year/list/";
			  }, function errorCallback(response) {
			    // called asynchronously if an error occurs
			    // or server returns response with an error status.
			  });
		}
		
	};
});