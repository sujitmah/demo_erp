coAccountApp.service('CoAccountService', function($http) {

	this.submitCoAccount = function(data){
		return $http({
            method: 'POST',
            data: data,
            url: '/erp-accounts/coaccounts/api/accountgroup/list/',
            headers: {
                'Content-Type': 'application/json'
            }
        });
	};
    this.submitLedger = function(data){
        return $http({
            method: 'POST',
            data: data,
            url: '/erp-accounts/coaccounts/api/ledger/list/',
            headers: {
                'Content-Type': 'application/json'
            }
        });
    };

    this.submitCompanyInfo = function(data){
        return $http({
            method: 'POST',
            data: data,
            url: '/erp-accounts/coaccounts/api/company/create/',
            headers: {
                'Content-Type': 'application/json'
            }
        });
    };

    this.addFiscalYear = function(data){
        return $http({
            method: 'POST',
            data: data,
            url: '/erp-accounts/coaccounts/api/fiscal-year/list/',
            headers: {
                'Content-Type': 'application/json'
            }
        });
    };

    this.updateCompanyInfo = function(data){
        return $http({
            method: 'PUT',
            data: data,
            url: '/erp-accounts/coaccounts/api/company/1',
            headers: {
                'Content-Type': 'application/json'
            }
        });
    };

    this.getCompanyInfo = function(data){
        return $http.get('/erp-accounts/coaccounts/api/company/1');
    };



});