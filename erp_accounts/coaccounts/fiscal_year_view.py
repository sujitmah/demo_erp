# Create your views here.
# import json
# import uuid

# import hashlib

# from django.http import Http404
# from django.views.generic import TemplateView
# from braces.views import LoginRequiredMixin
# from django.views.decorators.csrf import csrf_exempt
from django.views.generic import View
from django.core.urlresolvers import reverse
# from django.utils.decorators import method_decorator
# from django.core.context_processors import csrf
# from django.views.generic import View
# from braces.views import StaffuserRequiredMixin
from django.shortcuts import render_to_response, HttpResponse, HttpResponseRedirect
# from django.http import Http404, HttpResponseRedirect
from django.template import RequestContext
# from django.shortcuts import render
from django.views.generic import DetailView, CreateView, ListView, RedirectView, UpdateView
import json
from .models import Company, AccountGroup, FiscalYear


class AddFiscalYear(View):
    template_name = "coaccounts/fiscal_year_form.html"

    def get_context(self):
        params = {}
        return params
    # send the user back to their own page after a successful update

    def get(self, request, *args, **kwargs):
        latest_fiscal_year = FiscalYear.objects.last()
        # print(count)
        # if count > 0:
        #     return HttpResponseRedirect(reverse("coaccounts:update_company"))
        return render_to_response(
            self.template_name,
            self.get_context(),
            context_instance=RequestContext(request)
        )
