try:
    from django.conf.urls import *
except ImportError:  # django < 1.4
    from django.conf.urls.defaults import *

from .views import *

urlpatterns = [
    # URL pattern for the UserListView
    url(
        regex=r'^company/create/$',
        view=CreateCompany.as_view(),
        name='company_create'
    ),
    url(
        regex=r'^company/(?P<pk>[0-9]+)$',
        view=CompanyInfo.as_view(),
        name='company_info'
    ),
    url(
        regex=r'^fiscal-year/list/$',
        view=FiscalYearList.as_view(),
        name='fiscal_year_list'
    ),
    url(
        regex=r'^fiscal-year/(?P<pk>[0-9]+)$',
        view=FiscalYearAPI.as_view(),
        name='fiscal_year_update'
    ),
    url(
        regex=r'^accountgroup/list/$',
        view=AccountGroupList.as_view(),
        name='list_accountsgroups'
    ),
    url(
        regex=r'^accountgroup/(?P<pk>[0-9]+)$',
        view=AccountGroupAPI.as_view(),
        name='accountgroup_api'
    ),
    url(
        regex=r'^ledger/list/$',
        view=LedgerList.as_view(),
        name='list_ledger'
    ),
    url(
        regex=r'^ledger/$',
        view=LedgerAPI.as_view(),
        name='ledger_api'
    ),
]
# place app url patterns here
