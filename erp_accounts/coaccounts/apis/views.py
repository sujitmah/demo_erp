from rest_framework import generics
from django.shortcuts import get_object_or_404

from erp_accounts.coaccounts.apis.serializers import AccountGroupSerializer, LedgerSerializer, CompanySerializer, FiscalYearSerializer
from erp_accounts.coaccounts.models import AccountGroup, Ledger, Company, FiscalYear

class CreateCompany(generics.CreateAPIView):
    model = Company
    serializer_class = CompanySerializer


class CompanyInfo(generics.RetrieveUpdateDestroyAPIView):
    model = Company
    queryset = Company.objects.all()
    serializer_class = CompanySerializer
    def get_object(self):
        pk = int(self.kwargs.get('pk'))
        return get_object_or_404(self.queryset, pk=pk)

class FiscalYearList(generics.ListCreateAPIView):
    model = FiscalYear
    queryset = FiscalYear.objects.all()
    serializer_class = FiscalYearSerializer


class FiscalYearAPI(generics.RetrieveUpdateDestroyAPIView):
    model = FiscalYear
    serializer_class = FiscalYearSerializer
    queryset = FiscalYear.objects.all()

    def get_object(self):
        pk = int(self.kwargs.get('pk'))
        return get_object_or_404(self.queryset, pk=pk)



class AccountGroupList(generics.ListCreateAPIView):
    model = AccountGroup
    queryset = AccountGroup.objects.all()
    serializer_class = AccountGroupSerializer


class AccountGroupAPI(generics.RetrieveUpdateDestroyAPIView):
    model = AccountGroup
    serializer_class = AccountGroupSerializer
    queryset = AccountGroup.objects.all()

    def get_object(self):
        pk = int(self.kwargs.get('pk'))
        return get_object_or_404(self.queryset, pk=pk)


class LedgerList(generics.ListCreateAPIView):
    model = Ledger
    queryset = Ledger.objects.all()
    serializer_class = LedgerSerializer


class LedgerAPI(generics.RetrieveUpdateDestroyAPIView):
    model = Ledger
    serializer_class = LedgerSerializer
    queryset = Ledger.objects.all()

    def get_object(self):
        pk = int(self.kwargs.get('pk'))
        return get_object_or_404(self.queryset, pk=pk)

