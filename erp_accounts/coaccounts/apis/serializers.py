from rest_framework import serializers

from erp_accounts.coaccounts.models import AccountGroup, Ledger, Company, FiscalYear


# class TransactionSerializer(serializers.ModelSerializer):

#     class Meta:
#         model = Transaction
#         fields = ('transaction_type', 'amount')



class FiscalYearSerializer(serializers.ModelSerializer):

    class Meta:
        model = FiscalYear
        fields = ("id","fiscal_year_from","fiscal_year_to")


class AccountGroupSerializer(serializers.ModelSerializer):
    # fiscal_year = FiscalYearSerializer(read_only=True)
    
    class Meta:
        model = AccountGroup
        fields = ('id','name',"parent","report_balance","use_tax","purchase_invoice")

class LedgerSerializer(serializers.ModelSerializer):

    class Meta:
        model = Ledger
        fields = ('id','account_group','name',"opening_balance")

class CompanySerializer(serializers.ModelSerializer):
    class Meta:
        model = Company
        fields = ('id','name',"registration_no","pan_no","street","city","district","zone","country","currency_symbol","currency_formal_name",
            "decimal_places","decimal_word","decimal_printing_word","affixed_amount",
            "space_amount_symbol", "amount_millions")


    # def create(self, validated_data):
    #     transactions_data = validated_data.pop('transactions')
    #     journal = Journal.objects.create(**validated_data)
    #     for transaction in transactions_data:
    #         Transaction.objects.create(
    #             journal=journal, **transaction)
    #     return journal
