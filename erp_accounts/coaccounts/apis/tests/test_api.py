from rest_framework import status
from rest_framework.test import APITestCase


from django.core.urlresolvers import reverse

from ...models import AccountGroup, Ledger, FiscalYear


class LedgerCreateAPITests(APITestCase):
    """
    1. Load default data using fixtures.
    2. List groups.
    3. Select the group to create ledger
    4. Add other info and call

    """

    def setUp(self):
        import datetime
        FiscalYear.objects.create(
            fiscal_year_from=datetime.datetime.now().date(),
            fiscal_year_to=datetime.datetime.now().date(),
        )
        load_fixture_url = reverse("coaccounts:load_fixtures")
        data_to_post = {"q": "load"}
        self.client.get(load_fixture_url, data_to_post)

    def test_more_than_one_group_in_group_objects(self):
        self.assertGreater(AccountGroup.objects.count(), 20)

    def test_group_listing(self):
        url = reverse('coaccounts:coaccounts_apis:list_accountsgroups')
        response = self.client.get(url, {}, format='json')
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(response.data[0]['name'], 'Income')

    def test_post_create_grup(self):
        account_group = AccountGroup.objects.get(name='Direct Incomes')
        url = reverse('coaccounts:coaccounts_apis:list_accountsgroups')
        data = {
            "parent": account_group.id,
            "name": 'Class XI fee',
            "fiscal_year": FiscalYear.objects.all()[0].id
        }
        response = self.client.post(url, data, format='json')
        self.assertEqual(response.status_code, status.HTTP_201_CREATED)
        self.assertEqual(AccountGroup.objects.get(name='Class XI fee').name, 'Class XI fee')
        self.assertEqual(
            AccountGroup.objects.get(name='Class XI fee').parent.name, 'Direct Incomes')

    def test_post_create_ledger(self):
        account_group = AccountGroup.objects.get(name='Direct Incomes')
        url = reverse('coaccounts:coaccounts_apis:list_ledger')
        data = {
            "account_group": account_group.id,
            "name": 'Rent',
            "fiscal_year": FiscalYear.objects.all()[0].id
        }
        response = self.client.post(url, data, format='json')
        self.assertEqual(response.status_code, status.HTTP_201_CREATED)
        self.assertEqual(Ledger.objects.get(name='Rent').name, 'Rent')
        self.assertEqual(
            Ledger.objects.get(name='Rent').account_group.parent.name, 'Income')
