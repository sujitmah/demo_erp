from rest_framework import generics

from erp_accounts.coaccounts.apis.serializers import CoAccountsGroupSerializer
from erp_accounts.coaccounts.apis.models import CoAccountsGroup


class CoAccountsGroupList(generics.ListCreateAPIView):
    model = CoAccountsGroup
    queryset = CoAccountsGroup.objects.all()
    serializer_class = CoAccountsGroupSerializer


class JournalAPI(generics.RetrieveUpdateDestroyAPIView):
    model = Journal
    serializer_class = JournalSerializer
