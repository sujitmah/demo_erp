# Create your views here.
# import json
# import uuid

# import hashlib

# from django.http import Http404
# from django.views.generic import TemplateView
# from braces.views import LoginRequiredMixin
# from django.views.decorators.csrf import csrf_exempt
from django.views.generic import View
from django.core.urlresolvers import reverse
# from django.utils.decorators import method_decorator
# from django.core.context_processors import csrf
# from django.views.generic import View
# from braces.views import StaffuserRequiredMixin
from django.shortcuts import render_to_response, HttpResponse, HttpResponseRedirect
# from django.http import Http404, HttpResponseRedirect
from django.template import RequestContext
# from django.shortcuts import render
from django.views.generic import DetailView, CreateView, ListView, RedirectView, UpdateView
import json
from .models import Company, AccountGroup, FiscalYear

from .fiscal_year_view import *
class ReadCompanyInformation(View):
    template_name = "coaccounts/company_form.html"

    def get_context(self):
        params = {}
        return params
    # send the user back to their own page after a successful update

    def get(self, request, *args, **kwargs):
        count = Company.objects.all().count()
        print(count)
        if count > 0:
            return HttpResponseRedirect(reverse("coaccounts:update_company"))
        return render_to_response(
            self.template_name,
            self.get_context(),
            context_instance=RequestContext(request)
        )


class AddAccountGroup(View):
    template_name = "coaccounts/add_account_group.html"

    def get_context(self):
        params = {}
        return params

    def get(self, request, *args, **kwargs):
        return render_to_response(
            self.template_name,
            self.get_context(),
            context_instance=RequestContext(request)
        )

class AddAccountLedger(View):
    template_name = "coaccounts/add_account_ledger.html"

    # send the user back to their own page after a successful update
    def get(self, request, *args, **kwargs):
        return render_to_response(
            self.template_name,
            {},
            context_instance=RequestContext(request)
        )

class CompanyInfoDisplay(View):

    template_name = "coaccounts/display_company_info.html"

    # send the user back to their own page after a successful update
    def get(self, request, *args, **kwargs):
        return render_to_response(
            self.template_name,
            {},
            context_instance=RequestContext(request)
        )

class CompanyInfoUpdate(View):


    template_name = "coaccounts/update_company_info.html"
    
    # send the user back to their own page after a successful update
    def get(self, request, *args, **kwargs):
        return render_to_response(
            self.template_name,
            {},
            context_instance=RequestContext(request)
        )

class LoadFixtures(View):
    template_name = "coaccounts/chart_of_account_visualization.html"

    def load_fixture(self):
        import datetime
        FiscalYear.objects.create(
            fiscal_year_from=datetime.datetime.now().date(),
            fiscal_year_to=datetime.datetime.now().date(),
        )
        primary_groups = [
            'Income',
            'Expenses',
            'Assets',
            'Liabilities'
            ]
        secondary_groups ={
        "Assets":[
            'Bank Accounts',
            'Bank OCC A/C',
            'Bank OD A/C',
            'Branch / Divisions',
            'Cash-in-hand',
            'Current Assets',
            'Fixed Assets',
            'Deposits (Asset)',
            'Investments',
            'Misc. Expenses (ASSET)',
            'Retained Earnings',
            'Reserves & Surplus',
            'Stock-in-hand',
        ],

        "Liabilities":[
            'Capital Account',
            'Current Liabilities',
            'Loans & Advances (Asset)',
            'Loans (Liability)',
            'Provisions',
            'Sundry Creditors',
            'Sundry Debtors',
            'Unsecured Loans',
            'Secured Loans',
        ],
        "Income":[

            'Direct Incomes',
            'Income (Direct)',
            'Indirect Incomes',
            'Income (Indirect)',
            'Export',   

            'Sales Accounts',
            ],
        "Expenses":[
            'Direct Expenses',
            'Duties & Taxes',
            'Expenses (Direct)',
            'Expenses (Indirect)',
            'Indirect Expenses',
            'Suspense A/C',
            'Purchase Accounts',
            ]
            }
        for primary_group in primary_groups:
            fiscal_year = FiscalYear.objects.all()[0]
            primary_object = AccountGroup(name=primary_group,parent = None,fiscal_year=fiscal_year)
            primary_object.save()
            for secondary_group in secondary_groups[primary_group]:
                secondary_object = AccountGroup(name=secondary_group,parent = primary_object, fiscal_year=fiscal_year)
                secondary_object.save() 



    def get(self,request,*args,**kwargs):
        if request.GET.get("q") == "load":
            self.load_fixture()

        return render_to_response(
            self.template_name,
            {},
            context_instance=RequestContext(request)
        )
