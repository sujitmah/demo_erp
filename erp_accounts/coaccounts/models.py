# -*- coding: utf-8 -*-
from __future__ import unicode_literals, absolute_import
from django.contrib.auth.models import AbstractUser
from django.core.urlresolvers import reverse
from django.db import models
from django.utils.encoding import python_2_unicode_compatible
from django.utils.translation import ugettext_lazy as _


@python_2_unicode_compatible
class Company(models.Model):
    # First Name and Last Name do not cover name patterns
    # around the globe.
    name = models.CharField(_("Name of the company"), blank=True, max_length=255)
    registration_no = models.CharField(_("Registration No."), blank=True, max_length=255)
    pan_no = models.CharField(_("Pan no."), blank=True, max_length=255)
    street = models.CharField(_("Street"), blank=True, max_length=255)
    city = models.CharField(_("City"), blank=True, max_length=255)
    district = models.CharField(_("District"), blank=True, max_length=255)
    zone = models.CharField(_("Zone"), blank=True, max_length=255)
    country = models.CharField(_("Country"), blank=True, max_length=255)
    currency_symbol = models.CharField(_("Currency Symbol"), blank=True, max_length=255)
    currency_formal_name = models.CharField(_("Currency Formal Name"), blank=True, max_length=255)
    decimal_places = models.IntegerField(_("Decimal places for printing amount in words"),default=2)
    decimal_word = models.CharField(_("Word used to print decimal portion of amount"), default="paisa", max_length=255)
    decimal_printing_word = models.IntegerField(_("Decimal places for printing amount in words"),default=2)
    affixed_amount = models.BooleanField(_("Is symbol affixed to amount ?"),default=False)
    space_amount_symbol = models.BooleanField(_("Add space between amount and symbol"),default=False)
    amount_millions = models.BooleanField(_("Show amount in millions ?"),default=False)
    def __str__(self):
        return self.name


    def get_absolute_url(self):
        return reverse('coaccounts:company', kwargs={'company_name': self.name})

class FiscalYear(models.Model):
    """docstring for FinancialYear"""
    fiscal_year_from = models.DateField()
    fiscal_year_to = models.DateField()
    account_closed = models.BooleanField(default=False)
    
        
@python_2_unicode_compatible
class AccountGroup(models.Model):
    fiscal_year = models.ForeignKey(FiscalYear,related_name="account_groups")
    parent = models.ForeignKey("self", blank=True, null=True,related_name="children")
    name = models.CharField(_("Group Name"), max_length=50)
    sub_ledger = models.BooleanField(_("Group behaves like a Sub-Ledger ?"),default=False)
    report_balance = models.BooleanField(_("Nett Debit/Credit Balances for Reporting ?"),default=False)
    use_tax = models.BooleanField(_("Used for Calculation (eg. Taxes, Discounts) (for Sales Invoice Entry) ?"),default=False)
    purchase_invoice = models.BooleanField(_("Method to Allocate when used in Purchase Invoice ?"),default=False)
    
    def __str__(self):
        return self.name

    def save(self, *args, **kwargs):
        self.fiscal_year = FiscalYear.objects.filter(account_closed=False)[0]

       
        super(AccountGroup, self).save(*args, **kwargs)


    def get_absolute_url(self):
        return reverse('coaccounts:dashboard', kwargs={'group_name': self.group_name})


@python_2_unicode_compatible
class ChequeReciept(models.Model):

    received_from = models.CharField(_("Recieved from"), max_length=50)
    transaction_type = models.CharField(_("Transaction Type"), max_length=50)
    amount = models.CharField(_("Amount"), max_length=50)
    instrument_no = models.CharField(_("Instrument No."), max_length=50)
    received_from = models.CharField(_("Recieved from"), max_length=50)
    instrument_date = models.DateField(_("Instrument Date"))
    bank = models.CharField(_("Bank"), max_length=50)
    branch = models.CharField(_("Branch"), max_length=50)

    def __str__(self):
        return self.received_from

    # def get_absolute_url(self):
    #     return reverse('coaccounts:dashboard', kwargs={'group_name': self.group_name})


@python_2_unicode_compatible
class Ledger(models.Model):
    account_group = models.ForeignKey(AccountGroup,related_name="ledgers")
    name = models.CharField(_("Ledger"), max_length=50)
    opening_balance = models.DecimalField(default=0,max_digits=10, decimal_places=2)
    def __str__(self):
        return self.name

    def get_absolute_url(self):
        return reverse('coaccounts:dashboard', kwargs={'group_name': self.group_name})




@python_2_unicode_compatible
class Banks(models.Model):
    ledger = models.ForeignKey(Ledger,related_name="banks")
    name = models.CharField(_("Group Name"), max_length=50)
    account_no = models.CharField(_("Account Number"), max_length=50)
    account_holder_name  = models.CharField(_("Account Holder's Name"), max_length=50)
    branch_name = models.CharField(_("Branch Name"), max_length=50)
    branch_code = models.CharField(_("Branch Code"), max_length=50)

    def __str__(self):
        return self.name

    def get_absolute_url(self):
        return reverse('coaccounts:dashboard', kwargs={'group_name': self.group_name})

