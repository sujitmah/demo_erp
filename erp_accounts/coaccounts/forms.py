from django import forms
from erp_accounts.coaccounts.models import Company
# place form definition here


class UserForm(forms.ModelForm):
    class Meta:
        # Set this form to use the User model.
        model = Company

        # Constrain the UserForm to just these fields.
        # fields = ("first_name", "last_name")