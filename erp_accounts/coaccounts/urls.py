try:
    from django.conf.urls import *
except ImportError:  # django < 1.4
    from django.conf.urls.defaults import *

from .views import *

urlpatterns = [
    # URL pattern for the UserListView
    url(
        regex=r'^company/$',
        view=CompanyInfoDisplay.as_view(),
        name='display_company_information'
    ),
    url(
        regex=r'^company/create/$',
        view=ReadCompanyInformation.as_view(),
        name='create_company'
    ),
    url(
        regex=r'^company/update/$',
        view=CompanyInfoUpdate.as_view(),
        name='update_company'
    ),
    url(
        regex=r'^fiscal-year/add/$',
        view=AddFiscalYear.as_view(),
        name='add_fiscal_year'
    ),
    url(
        regex=r'^group/add/$',
        view=AddAccountGroup.as_view(),
        name='add_account_group'
    ),
   	url(
        regex=r'^ledger/add/$',
        view=AddAccountLedger.as_view(),
        name='add_account_ledger'
    ),
    url(
        regex=r'^load/fixtures/$',
        view=LoadFixtures.as_view(),
        name='load_fixtures'
    ),
    url(
        r'^api/',
        include(
            'erp_accounts.coaccounts.apis.urls',
            namespace="coaccounts_apis")
    ),
]
# place app url patterns here
