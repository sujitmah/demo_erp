from django.views.generic import CreateView, ListView, TemplateView
from django.core.urlresolvers import reverse

from .models import Journal


class JournalCreate(CreateView):
    model = Journal
    fields = ['description', ]


class JournalListView(ListView):
    model = Journal


class JournalUpdate(TemplateView):
    template_name = 'journal/journal_form.html'

    def get_context_data(self, **kwargs):
        # Call the base implementation first to get a context
        context = super(JournalUpdate, self).get_context_data(**kwargs)
        context['ajax_url'] = reverse('journal:journal-api', args=(kwargs.get('pk'), ))
        # Add in a QuerySet of all the books
        return context
