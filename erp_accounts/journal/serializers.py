from rest_framework import serializers

from django.db import transaction

from .models import Journal, Transaction, ChequeReceipt


class ChequeReceiptSerializer(serializers.ModelSerializer):

    class Meta:
        model = ChequeReceipt
        fields = (
            'id', 'transaction_type', 'amount',
            'instrument_no', 'received_from', 'instrument_date',
            'bank', 'branch'
        )


class TransactionSerializer(serializers.ModelSerializer):
    cheque_receipts = ChequeReceiptSerializer(
        many=True, required=False)

    class Meta:
        model = Transaction
        fields = ('id', 'transaction_type', 'amount', 'ledger', 'cheque_receipts')


class JournalSerializer(serializers.ModelSerializer):
    transactions = TransactionSerializer(
        many=True,
    )

    class Meta:
        model = Journal
        fields = ('id', 'description', 'transactions', 'journal_date')

    @transaction.atomic
    def create(self, validated_data):
        journal = Journal.create_journal(**validated_data)
        return journal

    @transaction.atomic
    def update(self, instance, validated_data):
        transactions_data = validated_data.pop('transactions')
        for (key, value) in validated_data.items():
            setattr(instance, key, value)
        instance.transactions.all().delete()
        for each_transaction in transactions_data:
            Transaction.create_transaction(
                journal=instance, **each_transaction)
        instance.save()
        return instance

    def validate(self, data):
        data = super(JournalSerializer, self).validate(data)
        total_debit = sum([
            i["amount"] for i in data['transactions']
            if i["transaction_type"] == "debit"
        ])
        total_credit = sum([
            i["amount"] for i in data['transactions']
            if i["transaction_type"] == "credit"
        ])
        if total_credit != total_debit:
            raise serializers.ValidationError("Debit is not Equal to Credit")
        return data
