# -*- coding: utf-8 -*-
from __future__ import absolute_import, unicode_literals

from django.conf.urls import url, include
from .views import JournalCreate, JournalListView, JournalUpdate
from .api import JournalList, JournalAPI

from django.views.generic import TemplateView

api_urls = [
    url(
        r'^list/$',
        JournalList.as_view(),
        name='journal-list-create-api'
    ),
    url(
        r'^update/(?P<pk>[0-9]+)/$',
        JournalAPI.as_view(),
        name='journal-api'
    ),
]


urlpatterns = [
    url(
        r'^create/$',
        JournalCreate.as_view(),
        name="journal-create"
    ),

    url(
        r'^list/$',
        JournalListView.as_view(),
        name="journal-list"
    ),

    url(
        r'^update/(?P<pk>[0-9]+)/$',
        JournalUpdate.as_view(),
        name="journal-update"
    ),

    # only for testing
    url(
        regex=r'^test/$',
        view=TemplateView.as_view
        (template_name='journal/test.html'),
        name="test"
    ),
    url(
        r'^api/',
        include(api_urls)
    ),

]
