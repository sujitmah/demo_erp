from django.db import models
from django.db.models import Sum
from django.utils.translation import ugettext_lazy as _

from model_utils.models import TimeStampedModel

from ..coaccounts.models import Ledger


class Journal(TimeStampedModel):
    description = models.CharField(max_length=100)
    journal_date = models.DateTimeField()

    @classmethod
    def create_journal(cls, **kwargs):
        transactions = kwargs.pop("transactions")
        journal = cls(**kwargs)
        journal.save()
        for each_transaction in transactions:
            Transaction.create_transaction(
                journal=journal,
                **each_transaction)
        return journal

    @property
    def total_debit(self):
        return self.transactions.filter(
            transaction_type="debit").aggregate(
            amount=Sum('amount')
        )

    @property
    def total_credit(self):
        return self.transactions.filter(
            journal=self, transaction_type="credit").aggregate(
                amount=Sum('amount')
        )


class Transaction(TimeStampedModel):
    transaction_type = models.CharField(max_length=100)
    ledger = models.ForeignKey(Ledger)
    amount = models.FloatField()
    journal = models.ForeignKey(Journal, related_name='transactions')

    @classmethod
    def create_transaction(cls, **kwargs):
        cheque_receipts = []
        if 'cheque_receipts' in kwargs:
            cheque_receipts = kwargs.pop("cheque_receipts")
        transaction = cls(**kwargs)
        transaction.save()
        if cheque_receipts:
            for each_cheque_receipt in cheque_receipts:
                ChequeReceipt.objects.create(
                    transaction=transaction, **each_cheque_receipt)
        return transaction

    # @property
    # def extra_field(self):
    #     if self.ledger == 'bank':
    #         return self.cheque_receipts
    #     else:
    #         return 0


class ChequeReceipt(models.Model):
    transaction = models.ForeignKey(
        Transaction, related_name='cheque_receipts')
    received_from = models.CharField(
        _("Recieved from"), max_length=50, blank=True)
    transaction_type = models.CharField(
        _("Transaction Type"), max_length=50, blank=True)
    amount = models.FloatField(
        _("Amount"), max_length=50)
    instrument_no = models.CharField(
        _("Instrument No."), max_length=50, blank=True)
    received_from = models.CharField(
        _("Recieved from"), max_length=50, blank=True)
    instrument_date = models.DateField(
        _("Instrument Date"), null=True)
    bank = models.CharField(
        _("Bank"), max_length=50, blank=True)
    branch = models.CharField(
        _("Branch"), max_length=50, blank=True)

    def __str__(self):
        return self.received_from
