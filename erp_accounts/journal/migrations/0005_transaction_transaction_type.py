# -*- coding: utf-8 -*-
# Generated by Django 1.9.1 on 2016-02-04 09:51
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('journal', '0004_auto_20160204_1234'),
    ]

    operations = [
        migrations.AddField(
            model_name='transaction',
            name='transaction_type',
            field=models.CharField(default='credit', max_length=100),
            preserve_default=False,
        ),
    ]
