from rest_framework import generics
from django.shortcuts import get_object_or_404


from .serializers import JournalSerializer
from .models import Journal


class JournalList(generics.ListCreateAPIView):
    model = Journal
    queryset = Journal.objects.all()
    serializer_class = JournalSerializer


class JournalAPI(generics.RetrieveUpdateDestroyAPIView):
    model = Journal
    serializer_class = JournalSerializer
    queryset = Journal.objects.all()

    def get_object(self):
        pk = int(self.kwargs.get('pk'))
        return get_object_or_404(self.queryset, pk=pk)
