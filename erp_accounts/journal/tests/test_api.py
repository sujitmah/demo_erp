from rest_framework import status
from rest_framework.test import APITestCase


from django.core.urlresolvers import reverse

from ..models import Journal, Transaction, ChequeReceipt

from ...coaccounts.models import Ledger, AccountGroup


class JournalCreateAPITests(APITestCase):

    data = {
        "transactions": [
            {
                "amount": 12000,
                "transaction_type": "debit",
                "ledger": 0
            }, {
                "amount": 12000,
                "transaction_type": "credit",
                "ledger": 0
            }
        ],
        "journal_date": "2016-02-05T18:15:00.000Z",
        "description": "Journal without cheque Receipt",
    }

    def setUp(self):
        load_fixture_url = reverse("coaccounts:load_fixtures")
        data_to_post = {"q": "load"}
        self.client.get(load_fixture_url, data_to_post)
        ledger_obj = Ledger.objects.create(
            account_group=AccountGroup.objects.get(
                name='Direct Incomes'))
        self.data['transactions'][0]['ledger'] = ledger_obj.pk
        self.data['transactions'][1]['ledger'] = ledger_obj.pk

    def test_create_journal_without_cheque_receipt(self):
        """
        Ensure we can can add a new journal.
        """
        url = reverse('journal:journal-list-create-api')
        response = self.client.post(url, self.data, format='json')
        self.assertEqual(response.status_code, status.HTTP_201_CREATED)
        self.assertEqual(Journal.objects.count(), 1)
        self.assertEqual(Journal.objects.get().description, 'Journal without cheque Receipt')

    def test_error_on_debit_not_equal_to_credit(self):
        url = reverse('journal:journal-list-create-api')
        self.data['transactions'][0]['amount'] -= 20
        response = self.client.post(url, self.data, format='json')
        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)
        self.assertEqual(response.data['non_field_errors'], ['Debit is not Equal to Credit'])
        self.assertEqual(Journal.objects.count(), 0)
        self.assertEqual(Transaction.objects.count(), 0)
        self.assertEqual(ChequeReceipt.objects.count(), 0)

    def test_create_journal_with_cheque_receipt(self):
        """
        Ensure we can can add a new journal with transactions and cheque_receipts.
        """
        url = reverse('journal:journal-list-create-api')
        self.data['transactions'][0]['cheque_receipts'] = [
            {
                "transaction_type": "Cheque Receipt",
                "amount": 12500,
                "instrument_no": "1212",
                "received_from": "Sudip Kafle",
                "instrument_date": "2016-02-04",
                "bank": "Himalayan Bank",
                "branch": "New Baneshwor"
            }
        ]
        self.data['description'] = 'Journal without cheque Receipt'

        response = self.client.post(url, self.data, format='json')
        self.assertEqual(response.status_code, status.HTTP_201_CREATED)
        self.assertEqual(Journal.objects.count(), 1)
        self.assertEqual(
            Journal.objects.get().transactions.first().cheque_receipts.first().amount, 12500)
        self.assertEqual(Journal.objects.get().description, 'Journal without cheque Receipt')


class JournalUpdateAPITests(APITestCase):

    data = {
        "transactions": [
            {
                "amount": 12000,
                "transaction_type": "debit",
                "id": 0,
                "ledger": 0
            }, {
                "amount": 12000,
                "transaction_type": "credit",
                "id": 1,
                "ledger": 0
            }
        ],
        "journal_date": "2016-02-05T18:15:00.000Z",
        "description": "Journal without cheque Receipt"
    }

    def setUp(self):
        load_fixture_url = reverse("coaccounts:load_fixtures")
        data_to_post = {"q": "load"}
        self.client.get(load_fixture_url, data_to_post)
        ledger_obj = Ledger.objects.create(
            account_group=AccountGroup.objects.get(
                name='Direct Incomes'))
        self.data['transactions'][0]['ledger'] = ledger_obj.pk
        self.data['transactions'][1]['ledger'] = ledger_obj.pk

    def test_update_journal_without_cheque_receipt(self):
        """
        Ensure we can can add a new journal.
        """
        create_url = reverse('journal:journal-list-create-api')
        response = self.client.post(create_url, self.data, format='json')
        self.assertEqual(response.status_code, status.HTTP_201_CREATED)
        self.assertEqual(Journal.objects.count(), 1)
        self.assertEqual(Journal.objects.get().description, 'Journal without cheque Receipt')

        pk = Journal.objects.get().pk
        update_url = reverse('journal:journal-api', args=(pk, ))
        self.data['description'] = 'Journal Description has been updated'
        response = self.client.put(update_url, self.data, format='json')
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(Journal.objects.get().description, 'Journal Description has been updated')
