# -*- coding: utf-8 -*-
from __future__ import absolute_import, unicode_literals

from django.conf.urls import include, url
from .views import DashboardView

urlpatterns = [
    # URL pattern for the UserListView
    url(
        regex=r'^$',
        view=DashboardView.as_view(),
        name='main_dashboard'
    ),
    url(
        r'^coaccounts/',
        include(
            'erp_accounts.coaccounts.urls',
            namespace="coaccounts")
    ),
    url(
        r'^journal/', include(
            'erp_accounts.journal.urls',
            namespace="journal")

    ),

]
