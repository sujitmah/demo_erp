# Create your views here.
import json
import uuid

import hashlib

from django.http import Http404
from django.views.generic import TemplateView
from braces.views import LoginRequiredMixin
from django.views.decorators.csrf import csrf_exempt
from django.views.generic import View
from django.utils.decorators import method_decorator
from django.core.context_processors import csrf
from django.views.generic import View
from braces.views import StaffuserRequiredMixin
from django.shortcuts import render_to_response, HttpResponse
from django.http import Http404, HttpResponseRedirect
from django.template import RequestContext
from django.shortcuts import render
from django.views.generic import DetailView, ListView, RedirectView, UpdateView

from erp.erp_accounts.coaccounts.models import Company

class ReadCompanyInformation(LoginRequiredMixin, UpdateView):
    fields = ['name', ]

    # we already imported User in the view code above, remember?
    model = Company

    # send the user back to their own page after a successful update
    def get_success_url(self):
        return reverse("users:detail",
                       kwargs={"username": self.request.user.username})

    def get_object(self):
        # Only get the User record for the user making the request
        return User.objects.get(username=self.request.user.username)
