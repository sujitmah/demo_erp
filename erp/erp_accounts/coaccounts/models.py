# -*- coding: utf-8 -*-
from __future__ import unicode_literals, absolute_import

from django.contrib.auth.models import AbstractUser
from django.core.urlresolvers import reverse
from django.db import models
from django.utils.encoding import python_2_unicode_compatible
from django.utils.translation import ugettext_lazy as _


@python_2_unicode_compatible
class Company(models.Model):

    # First Name and Last Name do not cover name patterns
    # around the globe.
    name = models.CharField(_("Name of the company"), blank=True, max_length=255)
    financial_year = models.CharField(_("Financial Year"), blank=True, max_length=255)
    country = models.CharField(_("Country Name"), blank=True, max_length=255)
    currency = models.CharField(_("Currency"), blank=True, max_length=255)
    state = models.CharField(_("State"), blank=True, max_length=255)
    city = models.CharField(_("City"), blank=True, max_length=255)
    pan_no = models.CharField(_("Pan no."), blank=True, max_length=255)

    def __str__(self):
        return self.name

    def get_absolute_url(self):
        return reverse('company:detail', kwargs={'company_name': self.name})
