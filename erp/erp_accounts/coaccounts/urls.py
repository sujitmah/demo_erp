try:
    from django.conf.urls import *
except ImportError:  # django < 1.4
    from django.conf.urls.defaults import *

from .views import ReadCompanyInformation

urlpatterns = [
    # URL pattern for the UserListView
    url(
        regex=r'^$',
        view=ReadCompanyInformation.as_view(),
        name='read_company_information'
    ),
]
# place app url patterns here
