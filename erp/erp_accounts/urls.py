# -*- coding: utf-8 -*-
from __future__ import absolute_import, unicode_literals

from django.conf.urls import include, url

urlpatterns = [
    # URL pattern for the UserListView
    url(r'^coaccounts/', include('erp.erp_accounts.coaccounts.urls')),

]
