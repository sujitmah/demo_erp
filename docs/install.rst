Install
=========

This is where you write how to get a new laptop to run this project.

With Docker:
-----------------

Use this documentation on [Docker](http://cookiecutter-django.readthedocs.org/en/latest/developing-locally-docker.html). You don't need docker machine on ubuntu or other linux.

Optionally, you can use this documentation [https://realpython.com/blog/python/development-and-deployment-of-cookiecutter-django-via-docker/](https://realpython.com/blog/python/development-and-deployment-of-cookiecutter-django-via-docker/)

1. Install docker
2. Install docker-compose
3. Initial code ::


	docker-compose -f dev.yml build
	docker-compose -f dev.yml up
	docker-compose -f dev.yml run django python manage.py migrate
	docker-compose -f dev.yml run django python manage.py createsuperuser

Use sudo if there is an error.

4. Testing your project ::
	
	docker-compose -f dev.yml run django coverage run manage.py test
	docker-compose -f dev.yml run django coverage html

Open htmlcov/index.html to see coverage results.





